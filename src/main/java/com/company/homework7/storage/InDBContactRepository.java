package com.company.homework7.storage;

import com.company.homework7.entity.Contact;
import com.company.homework7.service.InDBAuthService;
import com.company.homework7.utility.jdbc.JDBCTemplate;
import com.company.homework7.utility.jdbc.SQLContactConvertorFunction;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class InDBContactRepository implements ContactRepository {
    private final InDBAuthService authService;
    private final JDBCTemplate jdbcTemplate;

    @Override
    public boolean addContact(Contact contact) {
        jdbcTemplate.query(
                "insert into contacts(id,user_id, name , surname, second_name, type_contact , contact) " +
                        "values(?,?,?,?,?,?,?)",
                new Object[]{contact.getId(), authService.getAuthId(), contact.getName(), contact.getSurname(),
                        contact.getSecondName(), contact.getTypeContact().toValue(), contact.getContact()}
        );
        return true;
    }

    @Override
    public boolean deleteContact(String contact) {
        jdbcTemplate.query(
                "delete from contacts where contact = ? and user_id = ?;",
                new Object[]{contact, authService.getAuthId()}
        );
        return true;
    }

    @Override
    public List<Contact> getAllContactWithSortBySurname() {
        return jdbcTemplate.query(
                "select * from contacts where user_id = ? order by surname",
                new Object[]{authService.getAuthId()},
                new SQLContactConvertorFunction()
        );
    }

    @Override
    public List<Contact> getAllContactIsPhone() {
        return jdbcTemplate.query(
                "select * from contacts where user_id = ? and type_contact = 'PHONE'",
                new Object[]{authService.getAuthId()},
                new SQLContactConvertorFunction()
        );
    }

    @Override
    public List<Contact> getAllContactIsEmail() {
        return jdbcTemplate.query(
                "select * from contacts where user_id = ? and type_contact = 'EMAIL'",
                new Object[]{authService.getAuthId()},
                new SQLContactConvertorFunction()
        );
    }

    @Override
    public List<Contact> findContactByPartOfName(String name) {
        return jdbcTemplate.query(
                "select * from contacts where user_id = ? and name like ?",
                new Object[]{authService.getAuthId(), "%" + name + "%"},
                new SQLContactConvertorFunction()
        );
    }

    @Override
    public List<Contact> findContactByStartOfContact(String contact) {
        return jdbcTemplate.query(
                "select * from contacts where user_id = ? and contact like ?",
                new Object[]{authService.getAuthId(), "%" + contact + "%"},
                new SQLContactConvertorFunction()
        );
    }
}
