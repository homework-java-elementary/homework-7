package com.company.homework7.storage;

import com.company.homework7.dto.ResponseContact;
import com.company.homework7.dto.ResponseStatus;
import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;
import com.company.homework7.service.InNetworkAuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class InNetworkContactRepository implements ContactRepository{
    private final InNetworkAuthService networkAuthService;
    private final ObjectMapper objectMapper;
    private final HttpClient httpClient;
    private final Properties properties;

    @Override
    public boolean addContact(Contact contact) {
        String body = "{\"type\":\"" + contact.getTypeContact().getType() + "\",\"value\":\"" + contact.getContact()
                + "\",\"name\":\"" + contact.getName() + "\"}";

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .uri(URI.create(properties.getProperty("application.base.uri") + "contacts/add"))
                .setHeader("Authorization", "Bearer " + networkAuthService.getToken())
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponseStatus responseStatus = objectMapper.readValue(httpResponse.body(), ResponseStatus.class);
            if (responseStatus == ResponseStatus.ERROR) {
                System.out.println(responseStatus);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteContact(String contact) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Contact> getAllContactWithSortBySurname() {
        return getAllContacts().stream().
                sorted(Comparator.comparing(Contact::getName)).collect(Collectors.toList());
    }

    @Override
    public List<Contact> getAllContactIsPhone() {
        return getAllContacts().stream().
                filter(e -> e.getTypeContact() == TypeContact.PHONE).collect(Collectors.toList());
    }

    @Override
    public List<Contact> getAllContactIsEmail() {
        return getAllContacts().stream().
                filter(e -> e.getTypeContact() == TypeContact.EMAIL).collect(Collectors.toList());
    }

    @Override
    public List<Contact> findContactByPartOfName(String name) {
        String body = "{\"name\":\"" + name + "\"}";
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .uri(URI.create(properties.getProperty("application.base.uri") + "contacts/find"))
                .setHeader("Authorization", "Bearer " + networkAuthService.getToken())
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponseContact responseContact = objectMapper.readValue(httpResponse.body(), ResponseContact.class);
            return responseContact.getContacts();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Contact> findContactByStartOfContact(String contact) {
        String body = "{\"value\":\"" + contact + "\"}";
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .uri(URI.create(properties.getProperty("application.base.uri") + "contacts/find"))
                .setHeader("Authorization", "Bearer " + networkAuthService.getToken())
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponseContact responseContact = objectMapper.readValue(httpResponse.body(), ResponseContact.class);
            return responseContact.getContacts();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<Contact> getAllContacts(){
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(properties.getProperty("application.base.uri") + "contacts"))
                .setHeader("Authorization", "Bearer " + networkAuthService.getToken())
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponseContact responseContact = objectMapper.readValue(httpResponse.body(), ResponseContact.class);
            return responseContact.getContacts();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
