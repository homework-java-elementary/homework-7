package com.company.homework7.storage;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryContactRepository implements ContactRepository{
    private final List<Contact> list = new ArrayList<>();

    @Override
    public boolean addContact(Contact contact) {
        return list.add(contact);
    }

    @Override
    public boolean deleteContact(String contact) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getContact().equals(contact)) {
                list.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Contact> getAllContactWithSortBySurname() {
        return list.stream().sorted(Comparator.comparing(Contact::getName)).collect(Collectors.toList());
    }

    @Override
    public List<Contact> getAllContactIsPhone() {
        return list.stream().filter(e -> e.getTypeContact() == TypeContact.PHONE).collect(Collectors.toList());
    }

    @Override
    public List<Contact> getAllContactIsEmail() {
        return list.stream().filter(e -> e.getTypeContact() == TypeContact.EMAIL).collect(Collectors.toList());
    }

    @Override
    public List<Contact> findContactByPartOfName(String name) {
        return list.stream().filter(e -> e.getName().contains(name)).collect(Collectors.toList());
    }

    @Override
    public List<Contact> findContactByStartOfContact(String contact) {
        return list.stream().filter(e -> e.getContact().startsWith(contact)).collect(Collectors.toList());
    }
}
