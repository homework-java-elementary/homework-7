package com.company.homework7.storage;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;
import com.company.homework7.storage.contact.file.ContactFile;
import lombok.AllArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class InFileContactRepository implements ContactRepository {
    private ContactFile contactFile;

    @Override
    public boolean addContact(Contact contact) {
        List<Contact> contacts = contactFile.readAll();
        contacts.add(contact);
        contactFile.saveAll(contacts);
        return true;
    }

    @Override
    public boolean deleteContact(String contact) {
        List<Contact> list = contactFile.readAll().stream().
                filter(e -> !e.getContact().equals(contact)).
                collect(Collectors.toList());
        contactFile.saveAll(list);
        return true;
    }

    @Override
    public List<Contact> getAllContactWithSortBySurname() {
        return contactFile.readAll().stream().
                sorted(Comparator.comparing(Contact::getName)).collect(Collectors.toList());
    }

    @Override
    public List<Contact> getAllContactIsPhone() {
        return contactFile.readAll().stream().
                filter(e -> e.getTypeContact() == TypeContact.PHONE).collect(Collectors.toList());
    }

    @Override
    public List<Contact> getAllContactIsEmail() {
        return contactFile.readAll().stream().
                filter(e -> e.getTypeContact() == TypeContact.EMAIL).collect(Collectors.toList());
    }

    @Override
    public List<Contact> findContactByPartOfName(String name) {
        return contactFile.readAll().stream().
                filter(e -> e.getName().contains(name)).collect(Collectors.toList());
    }

    @Override
    public List<Contact> findContactByStartOfContact(String contact) {
        return contactFile.readAll().stream().
                filter(e -> e.getContact().startsWith(contact)).collect(Collectors.toList());
    }
}
