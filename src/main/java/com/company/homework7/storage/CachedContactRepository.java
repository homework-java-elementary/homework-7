package com.company.homework7.storage;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class CachedContactRepository implements ContactRepository {
    private final ContactRepository repository;
    private List<Contact> cacheAll;
    private Map<String, List<Contact>> cacheType = new HashMap<>();

    private void invalidateCache() {
        cacheAll = null;
        cacheType.clear();
    }

    @Override
    public boolean addContact(Contact contact) {
        invalidateCache();
        return repository.addContact(contact);
    }

    @Override
    public boolean deleteContact(String contact) {
        invalidateCache();
        return repository.deleteContact(contact);
    }

    @Override
    public List<Contact> getAllContactWithSortBySurname() {
        if (cacheAll == null) {
            cacheAll = repository.getAllContactWithSortBySurname();
        }
        return cacheAll;
    }

    @Override
    public List<Contact> getAllContactIsPhone() {
        if (!cacheType.containsKey(TypeContact.PHONE.getType())) {
            cacheType.put(TypeContact.PHONE.getType(), repository.getAllContactIsPhone());
        }
        return cacheType.get(TypeContact.PHONE.getType());
    }

    @Override
    public List<Contact> getAllContactIsEmail() {
        if (!cacheType.containsKey(TypeContact.EMAIL.getType())) {
            cacheType.put(TypeContact.EMAIL.getType(), repository.getAllContactIsEmail());
        }
        return cacheType.get(TypeContact.EMAIL.getType());
    }

    @Override
    public List<Contact> findContactByPartOfName(String name) {
        return repository.findContactByPartOfName(name);
    }

    @Override
    public List<Contact> findContactByStartOfContact(String contact) {
        return repository.findContactByStartOfContact(contact);
    }
}
