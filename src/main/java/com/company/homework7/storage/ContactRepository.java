package com.company.homework7.storage;

import com.company.homework7.entity.Contact;

import java.util.List;

public interface ContactRepository {
    boolean addContact(Contact contact);

    boolean deleteContact(String contact);

    List<Contact> getAllContactWithSortBySurname();

    List<Contact> getAllContactIsPhone();

    List<Contact> getAllContactIsEmail();

    List<Contact> findContactByPartOfName(String name);

    List<Contact> findContactByStartOfContact(String contact);
}
