package com.company.homework7.storage.contact.file;

import com.company.homework7.entity.Contact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ByteContactFile implements ContactFile{
    public static final String FILE_NAME = "Contacts.byte";

    @Override
    public List<Contact> readAll() {
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
            return (List<Contact>) is.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> list) {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
            os.writeObject(list);
            os.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
