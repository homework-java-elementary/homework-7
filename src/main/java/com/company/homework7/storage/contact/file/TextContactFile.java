package com.company.homework7.storage.contact.file;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TextContactFile implements ContactFile {
    public static final String FILE_NAME = "Contacts.txt";

    @Override
    public List<Contact> readAll() {
        List<Contact> result = new ArrayList<>();
        try (BufferedReader r = new BufferedReader(new FileReader(FILE_NAME))) {
            while (true) {
                String line = r.readLine();
                if (line == null) return result;
                String[] lines = line.split(",");
                Contact contact = new Contact(lines[1], lines[2], lines[3], getType(lines[4]), lines[5], lines[0]);
                result.add(contact);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> list) {
        try (PrintWriter w = new PrintWriter(new FileWriter(FILE_NAME))) {
            for (Contact contact : list) {
                w.printf("%s,%s,%s,%s,%s,%s\n", contact.getId(), contact.getName(), contact.getSurname(),
                        contact.getSecondName(), contact.getTypeContact(), contact.getContact());
            }
            w.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private TypeContact getType(String type) {
        if (type.equals("PHONE")) return TypeContact.PHONE;
        else return TypeContact.EMAIL;
    }
}
