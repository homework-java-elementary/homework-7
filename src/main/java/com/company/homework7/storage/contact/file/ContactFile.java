package com.company.homework7.storage.contact.file;

import com.company.homework7.entity.Contact;

import java.util.List;

public interface ContactFile {
    List<Contact> readAll();
    void saveAll(List<Contact> list);
}
