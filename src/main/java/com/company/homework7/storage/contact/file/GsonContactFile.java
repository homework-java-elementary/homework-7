package com.company.homework7.storage.contact.file;

import com.company.homework7.entity.Contact;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.RequiredArgsConstructor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class GsonContactFile implements ContactFile {
    public static final String FILE_NAME = "Contacts.json";
    private final Gson gson;

    @Override
    public List<Contact> readAll() {
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
            return gson.fromJson((String) is.readObject(), new TypeToken<ArrayList<Contact>>() {
            }.getType());
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> list) {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
            os.writeObject(gson.toJson(list));
            os.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
