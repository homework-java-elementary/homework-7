package com.company.homework7.service;

import com.company.homework7.dto.User;

public interface AuthService {
    void login(User user);

    void register(User user);

    boolean isAuth();
}
