package com.company.homework7.service;

import com.company.homework7.dto.User;
import com.company.homework7.exception.AuthorizationExeption;

public class InMemoryAuthService implements AuthService {
    private final static String LOGIN = "admin";
    private final static String PASSWORD = "1234";
    private boolean auth = false;

    @Override
    public void login(User user) {
        if (!user.getLogin().equals(LOGIN) || !user.getPassword().equals(PASSWORD)) {
            throw new AuthorizationExeption();
        }
        auth = true;
    }

    @Override
    public void register(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAuth() {
        return auth;
    }
}
