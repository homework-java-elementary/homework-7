package com.company.homework7.service;

import com.company.homework7.entity.Contact;
import com.company.homework7.storage.ContactRepository;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class ContactService {
    private final ContactRepository contactRepository;

    public boolean addContact(Contact contact) {
        return contactRepository.addContact(contact);
    }

    public boolean deleteContact(String contact) {
        return contactRepository.deleteContact(contact);
    }

    public List<Contact> getAllContactWithSortBySurname() {
        return contactRepository.getAllContactWithSortBySurname();
    }

    public List<Contact> getAllContactIsPhone() {
        return contactRepository.getAllContactIsPhone();
    }

    public List<Contact> getAllContactIsEmail() {
        return contactRepository.getAllContactIsEmail();
    }

    public List<Contact> findContactByPartOfName(String name) {
        return contactRepository.findContactByPartOfName(name);
    }

    public List<Contact> findContactByStartOfContact(String contact) {
        return contactRepository.findContactByStartOfContact(contact);
    }
}
