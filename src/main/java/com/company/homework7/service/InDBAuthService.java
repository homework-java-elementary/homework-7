package com.company.homework7.service;

import com.company.homework7.dto.User;
import com.company.homework7.utility.encoder.PasswordEncoder;
import com.company.homework7.utility.jdbc.JDBCTemplate;
import com.company.homework7.utility.jdbc.SQLConvertorFunction;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class InDBAuthService implements AuthService {
    private final JDBCTemplate jdbcTemplate;
    private final PasswordEncoder encoder;
    @Getter
    private int authId = -1;

    @Override
    public void login(User user) {
        List<com.company.homework7.entity.User> list = jdbcTemplate.query(
                "select * from users where login = ? and password = ?",
                new Object[]{user.getLogin(), encoder.encode(user.getPassword())},
                new SQLConvertorFunction<>(com.company.homework7.entity.User.class)
        );
        if (list.size() == 0) authId = -1;
        else authId = list.get(0).getId();
    }

    @Override
    public void register(User user) {
        jdbcTemplate.query(
                "insert into users(login, password) values(?,?)",
                new Object[]{user.getLogin(), encoder.encode(user.getPassword())}
        );
    }

    @Override
    public boolean isAuth() {
        return authId != -1;
    }
}
