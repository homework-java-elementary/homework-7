package com.company.homework7.service;

import com.company.homework7.dto.ResponseStatus;
import com.company.homework7.dto.ResponseToken;
import com.company.homework7.dto.ResponseUser;
import com.company.homework7.dto.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Properties;

@RequiredArgsConstructor
public class InNetworkAuthService implements AuthService {
    private final ObjectMapper objectMapper;
    private final HttpClient httpClient;
    private final Properties properties;
    @Getter
    private String token;

    @Override
    public void login(User user) {
        try {
            String body = objectMapper.writeValueAsString(user);
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(body))
                    .uri(URI.create(properties.getProperty("application.base.uri") + "login"))
                    .setHeader("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .build();

            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponseToken responseToken = objectMapper.readValue(httpResponse.body(), ResponseToken.class);
            token = responseToken.getToken();
            if (token == null) {
                System.out.println(responseToken.getError());
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void register(User user) {
        try {
            String body = objectMapper.writeValueAsString(user);
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(body))
                    .uri(URI.create(properties.getProperty("application.base.uri") + "register"))
                    .setHeader("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .build();

            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponseUser responseUser = objectMapper.readValue(httpResponse.body(), ResponseUser.class);
            if (responseUser.getStatus() == ResponseStatus.ERROR) {
                System.out.println(responseUser.getError());
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isAuth() {
        return token != null;
    }

    public List<User> getAllUsers() {
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(properties.getProperty("application.base.uri") + "users"))
                .build();

        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return objectMapper.readValue(httpResponse.body(), ResponseUser.class).getUsers();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
