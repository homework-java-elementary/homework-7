package com.company.homework7.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseUser {
    private ResponseStatus status;
    private List<User> users;
    private String error;

    public boolean isSuccessful() {
        return status == ResponseStatus.OK;
    }
}
