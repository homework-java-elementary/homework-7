package com.company.homework7.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseToken {
    private ResponseStatus status;
    private String token;
    private String error;
}
