package com.company.homework7.dto;

import com.company.homework7.entity.Contact;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseContact {
    private ResponseStatus status;
    private List<Contact> contacts;
    private String error;
}
