package com.company.homework7.factory;

import com.company.homework7.service.InDBAuthService;
import com.company.homework7.storage.ContactRepository;
import com.company.homework7.storage.InDBContactRepository;
import com.company.homework7.utility.encoder.MD5PasswordEncoder;
import com.company.homework7.utility.encoder.PasswordEncoder;
import com.company.homework7.utility.jdbc.JDBCTemplate;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class InDBContactRepositoryFactory implements ContactRepositoryFactory {
    private InDBAuthService authService;
    private DataSource dataSource;

    @Override
    public ContactRepository createContactRepository() {
        return new InDBContactRepository(createAuthService(),getJDBCTemplate());
    }

    @Override
    public InDBAuthService createAuthService() {
        if (authService == null) {
            authService = new InDBAuthService(getJDBCTemplate(),getPasswordEncoder());
        }
        return authService;
    }

    private JDBCTemplate getJDBCTemplate() {
        return new JDBCTemplate(getDataSource());
    }

    private DataSource getDataSource() {
        if (dataSource == null) {
            HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/ContactBook");
            hikariConfig.setUsername("postgres");
            hikariConfig.setPassword("1234");
            dataSource = new HikariDataSource(hikariConfig);
        }
        return dataSource;
    }

    private PasswordEncoder getPasswordEncoder(){
        return new MD5PasswordEncoder();
    }
}
