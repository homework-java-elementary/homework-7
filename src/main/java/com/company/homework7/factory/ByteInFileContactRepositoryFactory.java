package com.company.homework7.factory;

import com.company.homework7.service.AuthService;
import com.company.homework7.service.InMemoryAuthService;
import com.company.homework7.storage.CachedContactRepository;
import com.company.homework7.storage.ContactRepository;
import com.company.homework7.storage.InFileContactRepository;
import com.company.homework7.storage.contact.file.ByteContactFile;
import com.company.homework7.storage.contact.file.ContactFile;

public class ByteInFileContactRepositoryFactory implements ContactRepositoryFactory {
    @Override
    public ContactRepository createContactRepository() {
        ContactFile contactFile = new ByteContactFile();
        return new CachedContactRepository(new InFileContactRepository(contactFile));
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }
}
