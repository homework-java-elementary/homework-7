package com.company.homework7.factory;

import com.company.homework7.Main;
import com.company.homework7.service.InNetworkAuthService;
import com.company.homework7.storage.CachedContactRepository;
import com.company.homework7.storage.ContactRepository;
import com.company.homework7.storage.InNetworkContactRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileReader;
import java.io.IOException;
import java.net.http.HttpClient;
import java.util.Optional;
import java.util.Properties;

public class InNetworkContactRepositoryFactory implements ContactRepositoryFactory {
    private InNetworkAuthService networkAuthService;

    @Override
    public ContactRepository createContactRepository() {
        InNetworkAuthService authService = createAuthService();
        return new CachedContactRepository(new InNetworkContactRepository(authService,createObjectMapper(),
                createHttpClient(), getProperties()));
    }

    @Override
    public InNetworkAuthService createAuthService() {
        if (networkAuthService == null) {
            networkAuthService = new InNetworkAuthService(createObjectMapper(), createHttpClient(), getProperties());
        }
        return networkAuthService;
    }

    private ObjectMapper createObjectMapper() {
        return new ObjectMapper();
    }

    private HttpClient createHttpClient() {
        return HttpClient.newBuilder().build();
    }

    private Properties getProperties(){
        String propertiesSource = System.getProperty("property.source");
        propertiesSource = Optional.ofNullable(propertiesSource).orElse("CLASSPATH");

        Properties properties = new Properties();
        try {
            if (propertiesSource.equals("CLASSPATH")) {
                properties.load(Main.class.getClassLoader().getResourceAsStream("config.properties"));
            } else {
                properties.load(new FileReader(propertiesSource));
            }
        } catch (IOException e) {
            properties.setProperty("application.base.uri", "IN_MEMORY");
        }

        return properties;
    }
}
