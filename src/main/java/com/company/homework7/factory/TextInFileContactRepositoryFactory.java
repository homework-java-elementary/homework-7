package com.company.homework7.factory;

import com.company.homework7.service.AuthService;
import com.company.homework7.service.InMemoryAuthService;
import com.company.homework7.storage.CachedContactRepository;
import com.company.homework7.storage.ContactRepository;
import com.company.homework7.storage.InFileContactRepository;
import com.company.homework7.storage.contact.file.ContactFile;
import com.company.homework7.storage.contact.file.TextContactFile;

public class TextInFileContactRepositoryFactory implements ContactRepositoryFactory {
    @Override
    public ContactRepository createContactRepository() {
        ContactFile contactFile = new TextContactFile();
        return new CachedContactRepository(new InFileContactRepository(contactFile));
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }
}
