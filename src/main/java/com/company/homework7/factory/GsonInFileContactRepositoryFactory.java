package com.company.homework7.factory;

import com.company.homework7.service.AuthService;
import com.company.homework7.service.InMemoryAuthService;
import com.company.homework7.storage.CachedContactRepository;
import com.company.homework7.storage.ContactRepository;
import com.company.homework7.storage.InFileContactRepository;
import com.company.homework7.storage.contact.file.ContactFile;
import com.company.homework7.storage.contact.file.GsonContactFile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonInFileContactRepositoryFactory implements ContactRepositoryFactory {
    @Override
    public ContactRepository createContactRepository() {
        ContactFile contactFile = new GsonContactFile(createGson());
        return new CachedContactRepository(new InFileContactRepository(contactFile));
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }

    private Gson createGson() {
        return new GsonBuilder()
                .serializeNulls()
                .create();
    }
}
