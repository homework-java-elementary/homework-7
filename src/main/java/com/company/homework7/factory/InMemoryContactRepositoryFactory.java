package com.company.homework7.factory;

import com.company.homework7.service.AuthService;
import com.company.homework7.service.InMemoryAuthService;
import com.company.homework7.storage.ContactRepository;
import com.company.homework7.storage.InMemoryContactRepository;

public class InMemoryContactRepositoryFactory implements ContactRepositoryFactory {
    @Override
    public ContactRepository createContactRepository() {
        return new InMemoryContactRepository();
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }
}
