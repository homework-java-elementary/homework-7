package com.company.homework7.factory;

import com.company.homework7.service.AuthService;
import com.company.homework7.storage.ContactRepository;

public interface ContactRepositoryFactory {
    ContactRepository createContactRepository();
    AuthService createAuthService();
}
