package com.company.homework7.exception;

public class AuthorizationExeption extends RuntimeException{
    @Override
    public String getMessage() {
        return "Incorrect login or password";
    }
}
