package com.company.homework7.exception;

public class InvalidModeConfiguration extends RuntimeException {
    @Override
    public String getMessage() {
        return "This validator only validates email";
    }
}
