package com.company.homework7;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;
import com.company.homework7.exception.InvalidModeConfiguration;
import com.company.homework7.factory.*;
import com.company.homework7.service.AuthService;
import com.company.homework7.service.ContactService;
import com.company.homework7.ui.menu.Menu;
import com.company.homework7.ui.menu.MenuItem;
import com.company.homework7.ui.menu.items.*;
import com.company.homework7.ui.view.ContactView;
import com.company.homework7.ui.view.UserView;
import com.company.homework7.utility.validator.Validator;
import com.company.homework7.utility.validator.contact.ContactValidator;
import com.company.homework7.utility.validator.contact.RegExpEmailValidator;
import com.company.homework7.utility.validator.contact.RegExpPhoneValidator;


import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Scanner;

public class Main {
    public static final String APPLICATION_MODE_KEY = "application.mode";

    public static void main(String[] args) {
        ContactRepositoryFactory contactRepositoryFactory = getContactRepositoryFactory();
        ContactService contactService = new ContactService(contactRepositoryFactory.createContactRepository());
        AuthService authService = contactRepositoryFactory.createAuthService();
        Scanner scanner = new Scanner(System.in);
        Validator<Contact> contactValidator = new ContactValidator(new RegExpPhoneValidator(),
                new RegExpEmailValidator()
        );
        ContactView contactView = new ContactView(scanner, contactValidator);
        UserView userView = new UserView(scanner);
        Menu menu = new Menu(new MenuItem[]{
                new AuthorizationMenuItem(userView, authService),
                new RegisterMenuItem(userView, authService),
                new AddContactMenuItem(contactService, contactView, authService),
                new ViewContactSortByNameMenuItem(contactService, contactView, authService),
                new ViewContactIsPhoneMenuItem(contactService, contactView, authService),
                new ViewContactIsEmailMenuItem(contactService, contactView, authService),
                new FindContactByPartOfNameMenuItem(contactService, contactView, authService),
                new FindContactByStartOfContactMenuItem(contactService, contactView, authService),
                new DeleteContactMenuItem(contactService, contactView, authService),
                new ExitMenuItem()
        }, scanner);
        menu.execute();
    }

    private static ContactRepositoryFactory getContactRepositoryFactory() {
        Properties properties = getProperties();
        String mode = properties.getProperty(APPLICATION_MODE_KEY);
        Map<String, ContactRepositoryFactory> factories = Map.of(
                "IN_MEMORY", new InMemoryContactRepositoryFactory(),
                "IN_TEXT_FILE", new TextInFileContactRepositoryFactory(),
                "IN_BYTE_FILE", new ByteInFileContactRepositoryFactory(),
                "IN_GSON_FILE", new GsonInFileContactRepositoryFactory(),
                "IN_DB", new InDBContactRepositoryFactory(),
                "IN_NETWORK", new InNetworkContactRepositoryFactory()
        );

        if (!factories.containsKey(mode)) {
            throw new InvalidModeConfiguration();
        }

        return factories.get(mode);
    }

    private static Properties getProperties() {
        String propertiesSource = System.getProperty("property.source");
        propertiesSource = Optional.ofNullable(propertiesSource).orElse("CLASSPATH");

        Properties properties = new Properties();
        try {
            if (propertiesSource.equals("CLASSPATH")) {
                properties.load(Main.class.getClassLoader().getResourceAsStream("config.properties"));
            } else {
                properties.load(new FileReader(propertiesSource));
            }
        } catch (IOException e) {
            properties.setProperty(APPLICATION_MODE_KEY, "IN_MEMORY");
        }

        return properties;
    }
}
