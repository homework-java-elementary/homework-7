package com.company.homework7.utility.validator.contact;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;
import com.company.homework7.exception.ContactIsEmailException;
import com.company.homework7.utility.validator.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpPhoneValidator implements Validator<Contact> {
    private static final Pattern PHONE_PATTERN = Pattern.compile("\\b(\\+380|0)(\\d{9})\\b");

    @Override
    public String isValid(Contact contact) {
        if (contact.getTypeContact() == TypeContact.EMAIL) throw new ContactIsEmailException();
        Matcher matcher = PHONE_PATTERN.matcher(contact.getContact());
        if (matcher.matches()) return "0" + matcher.group(2);
        return null;
    }
}
