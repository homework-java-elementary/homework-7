package com.company.homework7.utility.validator.contact;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;
import com.company.homework7.utility.validator.Validator;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ContactValidator implements Validator<Contact> {
    private Validator<Contact> phone;
    private Validator<Contact> email;

    @Override
    public String isValid(Contact contact) {
        if (contact.getTypeContact() == TypeContact.PHONE) return phone.isValid(contact);
        return email.isValid(contact);
    }
}
