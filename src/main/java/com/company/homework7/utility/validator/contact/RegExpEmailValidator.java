package com.company.homework7.utility.validator.contact;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;
import com.company.homework7.exception.ContactIsPhoneException;
import com.company.homework7.utility.validator.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpEmailValidator implements Validator<Contact> {
    private static final Pattern EMAIL_PATTERN = Pattern.compile("\\b(\\w|\\.)+@(\\w+\\.)+(com|ua|ru)\\b");

    @Override
    public String isValid(Contact contact) {
        if (contact.getTypeContact() == TypeContact.PHONE) throw new ContactIsPhoneException();
        Matcher matcher = EMAIL_PATTERN.matcher(contact.getContact());
        if (matcher.matches()) return matcher.group();
        return null;
    }
}
