package com.company.homework7.utility.validator;

public interface Validator<T> {
    String isValid(T object);
}
