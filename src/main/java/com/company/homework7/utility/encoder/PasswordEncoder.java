package com.company.homework7.utility.encoder;

public interface PasswordEncoder {
    String encode(String password);
    boolean verify(String password, String hash);
}
