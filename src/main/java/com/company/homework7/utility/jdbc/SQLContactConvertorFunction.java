package com.company.homework7.utility.jdbc;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

public class SQLContactConvertorFunction implements Function<ResultSet, Contact> {
    @Override
    public Contact apply(ResultSet rs) {
        try {
            if (rs.getString("type_contact").equals("PHONE")) {
                return new Contact()
                        .setContact(rs.getString("contact"))
                        .setTypeContact(TypeContact.PHONE)
                        .setSurname(rs.getString("surname"))
                        .setName(rs.getString("name"))
                        .setId(rs.getString("id"))
                        .setSecondName(rs.getString("second_name"));
            } else {
                return new Contact()
                        .setContact(rs.getString("contact"))
                        .setTypeContact(TypeContact.EMAIL)
                        .setSurname(rs.getString("surname"))
                        .setName(rs.getString("name"))
                        .setId(rs.getString("id"))
                        .setSecondName(rs.getString("second_name"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }
}
