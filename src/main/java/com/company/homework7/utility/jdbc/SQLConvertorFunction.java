package com.company.homework7.utility.jdbc;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.function.Function;

@RequiredArgsConstructor
public class SQLConvertorFunction<T> implements Function<ResultSet, T> {
    private final Class<T> clazz;

    @Override
    public T apply(ResultSet resultSet) {
        Field[] fields = clazz.getDeclaredFields();
        try {
            T instance = clazz.getConstructor().newInstance();

            for (Field field : fields) {
                String name = camelToSnake(field.getName());
                Object value = resultSet.getObject(name, field.getType());
                field.setAccessible(true);
                field.set(instance, value);
            }

            return instance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String camelToSnake(String str) {
        String result = "";

        char c = str.charAt(0);
        result = result + Character.toLowerCase(c);

        for (int i = 1; i < str.length(); i++) {

            char ch = str.charAt(i);
            if (Character.isUpperCase(ch)) {
                result = result + '_';
                result
                        = result
                        + Character.toLowerCase(ch);
            } else {
                result = result + ch;
            }
        }
        return result;
    }
}
