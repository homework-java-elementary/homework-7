package com.company.homework7.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.UUID;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact implements Serializable {
    private String id;
    private String name;
    private String surname;
    private String secondName;
    @JsonProperty("type")
    private TypeContact typeContact;
    @JsonProperty("value")
    private String contact;

    public Contact(String name, String surname, String secondName, TypeContact typeContact, String contact) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.typeContact = typeContact;
        this.contact = contact;
    }
    public Contact(){

    }

    public Contact(String name, String surname, String secondName, TypeContact typeContact, String contact, String id) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.typeContact = typeContact;
        this.contact = contact;
    }
}
