package com.company.homework7.entity;

import lombok.Data;

@Data
public class User {
    private String login;
    private String password;
    private Integer id;
}
