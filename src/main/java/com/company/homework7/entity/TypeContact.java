package com.company.homework7.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum TypeContact {
    PHONE("Phone"), EMAIL("Email");
    private final String type;

    TypeContact(String type) {
        this.type = type;
    }

    @JsonCreator
    public static TypeContact forValue(String value) {
        for (TypeContact typeContact : TypeContact.values()) {
            if (typeContact.toValue().equalsIgnoreCase(value)) {
                return typeContact;
            }
        }
        return null;
    }

    @JsonValue
    public String toValue() {
        return name();
    }
}
