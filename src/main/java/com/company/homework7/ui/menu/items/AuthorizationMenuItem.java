package com.company.homework7.ui.menu.items;

import com.company.homework7.service.AuthService;
import com.company.homework7.ui.menu.MenuItem;
import com.company.homework7.ui.view.UserView;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AuthorizationMenuItem implements MenuItem {
    private UserView userView;
    private AuthService authService;

    @Override
    public String getName() {
        return "Authorization";
    }

    @Override
    public void execute() {
        authService.login(userView.readUserForAuthorization());
    }
}
