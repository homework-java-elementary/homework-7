package com.company.homework7.ui.menu.items;

import com.company.homework7.service.AuthService;
import com.company.homework7.ui.menu.MenuItem;
import com.company.homework7.ui.view.UserView;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RegisterMenuItem implements MenuItem {
    private UserView userView;
    private AuthService authService;

    @Override
    public String getName() {
        return "Register new user";
    }

    @Override
    public void execute() {
        authService.register(userView.readUserForRegister());
    }
}
