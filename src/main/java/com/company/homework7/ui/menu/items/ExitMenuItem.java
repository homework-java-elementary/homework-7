package com.company.homework7.ui.menu.items;

import com.company.homework7.ui.menu.MenuItem;

public class ExitMenuItem implements MenuItem {
    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void execute() {
        System.out.println("Shutting down the program ...");
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}
