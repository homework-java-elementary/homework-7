package com.company.homework7.ui.menu;

public interface MenuItem {
    String getName();

    void execute();

    default boolean isFinal() {
        return false;
    }

    default boolean isVisible() {
        return true;
    }
}
