package com.company.homework7.ui.menu.items;

import com.company.homework7.service.AuthService;
import com.company.homework7.service.ContactService;
import com.company.homework7.ui.menu.MenuItem;
import com.company.homework7.ui.view.ContactView;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FindContactByPartOfNameMenuItem implements MenuItem {
    private ContactService contactService;
    private ContactView contactView;
    private AuthService authService;

    @Override
    public String getName() {
        return "Find contacts by part of name";
    }

    @Override
    public void execute() {
        contactView.show(contactService.findContactByPartOfName(contactView.getStringFromConsole("Enter part of name: ")));
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
