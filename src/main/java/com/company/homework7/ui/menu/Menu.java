package com.company.homework7.ui.menu;

import java.util.Scanner;

public class Menu {
    MenuItem[] items;
    Scanner scanner;
    int countVisible;

    public Menu(MenuItem[] items, Scanner scanner) {
        this.items = items;
        this.scanner = scanner;
    }

    public void execute() {
        while (true) {
            showMenu();
            int choice = getChoice();
            if (choice < 0 || choice >= countVisible) {
                System.out.println("Incorrect choice");
                continue;
            }
            choice = correctiveChoice(choice);
            items[choice].execute();
            if (items[choice].isFinal()) {
                break;
            }
        }
    }

    private void showMenu() {
        System.out.println("----------------------------------------");
        countVisible = 0;
        for (int i = 0; i < items.length; i++) {
            if (items[i].isVisible()) {
                System.out.printf("%2d - %s\n", countVisible + 1, items[i].getName());
                countVisible++;
            }
        }
        System.out.println("----------------------------------------");
    }

    private int getChoice() {
        System.out.print("Select menu item: ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        return choice - 1;
    }

    private int correctiveChoice(int number) {
        int curVisible = 0;
        for (int i = 0; i < items.length; i++) {
            if (items[i].isVisible()) curVisible++;
            if (curVisible == number + 1) return i;
        }
        return items.length;
    }
}
