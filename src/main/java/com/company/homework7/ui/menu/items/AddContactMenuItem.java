package com.company.homework7.ui.menu.items;

import com.company.homework7.service.AuthService;
import com.company.homework7.service.ContactService;
import com.company.homework7.ui.menu.MenuItem;
import com.company.homework7.ui.view.ContactView;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AddContactMenuItem implements MenuItem {
    private ContactService contactService;
    private ContactView contactView;
    private AuthService authService;

    @Override
    public String getName() {
        return "Add contact";
    }

    @Override
    public void execute() {
        contactService.addContact(contactView.readContact());
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
