package com.company.homework7.ui.view;

import com.company.homework7.dto.User;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Scanner;

@AllArgsConstructor
public class UserView {
    private Scanner scanner;

    public void show(List<User> list) {
        System.out.println("---------------------------User---------------------------");
        for (int i = 0; i < list.size(); i++) {
            show(list.get(i));
        }
        System.out.println("---------------------------User---------------------------");
        System.out.println();
    }

    public void show(User user) {
        System.out.println(user);
    }

    public User readUserForRegister() {
        String login = getStringFromConsole("Enter login: ");
        String dateBorn = getDateFromConsole("Enter date of born: ");
        String password = getStringFromConsole("Enter password: ");
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setDateBorn(dateBorn);
        return user;
    }

    public User readUserForAuthorization() {
        String login = getStringFromConsole("Enter login: ");
        String password = getStringFromConsole("Enter password: ");
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        return user;
    }

    private String getStringFromConsole(String text) {
        String string = "";
        while (string.length() < 1) {
            System.out.print(text);
            string = scanner.nextLine();
        }
        return string;
    }

    private String getDateFromConsole(String text) {
        String date = getStringFromConsole(text);
        return date;
    }
}
