package com.company.homework7.ui.view;

import com.company.homework7.entity.Contact;
import com.company.homework7.entity.TypeContact;
import com.company.homework7.utility.validator.Validator;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Scanner;

@AllArgsConstructor
public class ContactView {
    private Scanner scanner;
    private Validator<Contact> contactValidator;

    public void show(List<Contact> list) {
        System.out.println("---------------------------Contact---------------------------");
        for (int i = 0; i < list.size(); i++) {
            show(list.get(i));
        }
        System.out.println("---------------------------Contact---------------------------");
        System.out.println();
    }

    public void show(Contact contact) {
        System.out.println(contact);
    }

    public Contact readContact() {
        String name = getStringFromConsole("Enter name: ");
        String surname = getStringFromConsole("Enter surname: ");
        String secondName = getStringFromConsole("Enter second name: ");
        TypeContact typeContact = getTypeContactFromConsole();
        String contact = getContactFromConsole(typeContact);
        return new Contact(name, surname, secondName, typeContact, contact);
    }

    public String getStringFromConsole(String text) {
        String string = "";
        while (string.length() < 1) {
            System.out.print(text);
            string = scanner.nextLine();
        }
        return string;
    }

    private TypeContact getTypeContactFromConsole() {
        int answer = 0;
        while (answer != 1 && answer != 2) {
            System.out.print("Enter type contact 1 - phone, 2 - email: ");
            while (!scanner.hasNextInt()) {
                scanner.nextLine();
                System.out.print("Enter type contact 1 - phone, 2 - email: ");
            }
            answer = scanner.nextInt();
            scanner.nextLine();
        }
        if (answer == 1) {
            return TypeContact.PHONE;
        }
        return TypeContact.EMAIL;
    }

    private String getContactFromConsole(TypeContact typeContact) {
        Contact contact = new Contact("", "", "", typeContact, "");
        String resultContact = null;
        if (typeContact == TypeContact.PHONE) {
            do {
                System.out.println("For example 0xxyyyywww or +380xxyyyywww");
                contact.setContact(getStringFromConsole("Enter phone: "));
                resultContact = contactValidator.isValid(contact);
            } while (resultContact == null);
            return resultContact;
        }
        do {
            System.out.println("For example xxxxx@gmail.com");
            contact.setContact(getStringFromConsole("Enter email: "));
            resultContact = contactValidator.isValid(contact);
        } while (resultContact == null);
        return resultContact;
    }
}
