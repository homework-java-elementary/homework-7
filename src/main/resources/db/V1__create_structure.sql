CREATE TABLE users(
id SERIAL PRIMARY KEY,
login VARCHAR(30) NOT NULL UNIQUE,
password CHAR(32) NOT NULL
);

CREATE TABLE contacts(
id CHAR(36) PRIMARY KEY,
user_id INT NOT NULL REFERENCES users(id) on delete cascade,
name VARCHAR(20) NOT NULL,
surname VARCHAR(20) NOT NULL,
second_name VARCHAR(20) NOT NULL,
type_contact CHAR(5) NOT NULL,
contact VARCHAR(30) NOT NULL
);